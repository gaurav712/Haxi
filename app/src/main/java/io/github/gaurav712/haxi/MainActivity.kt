package io.github.gaurav712.haxi

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import com.google.firebase.FirebaseException
import com.google.firebase.FirebaseTooManyRequestsException
import com.google.firebase.auth.*
import io.github.gaurav712.haxi.databinding.ActivityMainBinding
import java.util.concurrent.TimeUnit.SECONDS

class MainActivity : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth
    private lateinit var phoneVerificationCallbacks: PhoneAuthProvider.OnVerificationStateChangedCallbacks
    private lateinit var binding: ActivityMainBinding
    private lateinit var verificationId: String
    private lateinit var forceResendingToken: PhoneAuthProvider.ForceResendingToken

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.phoneNumberLayout.visibility = View.VISIBLE
        binding.verificationCodeLayout.visibility = View.GONE

        auth = FirebaseAuth.getInstance()
        phoneVerificationCallbacks = object : PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

            override fun onVerificationCompleted(credential: PhoneAuthCredential) {
                verifyPhoneNumberWithCredential(credential)
            }

            override fun onVerificationFailed(e: FirebaseException) {
                if (e is FirebaseAuthInvalidCredentialsException) {
                    Toast.makeText(applicationContext, "Invalid Credentials!", Toast.LENGTH_SHORT).show()
                } else if (e is FirebaseTooManyRequestsException) {
                    Toast.makeText(applicationContext, "SMS Quota Exceeded!", Toast.LENGTH_SHORT).show()
                }
            }

            override fun onCodeSent(id: String, token: PhoneAuthProvider.ForceResendingToken) {
                super.onCodeSent(id, token)

                verificationId = id
                forceResendingToken = token

                binding.phoneNumberLayout.visibility = View.GONE
                binding.verificationCodeLayout.visibility = View.VISIBLE
            }
        }

        binding.verificationCodeSubmitButton.setOnClickListener {
            val code = binding.verificationCodeEditText.text.toString().trim()
            val credential = PhoneAuthProvider.getCredential(verificationId, code)

            verifyPhoneNumberWithCredential(credential)
        }

        binding.submitButton.setOnClickListener {
            val options = PhoneAuthOptions.newBuilder(auth)
                .setPhoneNumber(binding.phoneNumberEditText.text.toString())
                .setTimeout(60L, SECONDS)
                .setActivity(this)
                .setCallbacks(phoneVerificationCallbacks)
                .build()

            PhoneAuthProvider.verifyPhoneNumber(options)
        }

        binding.resendVerificationCodeTextView.setOnClickListener {
            val options = PhoneAuthOptions.newBuilder(auth)
                .setPhoneNumber(binding.phoneNumberEditText.text.toString())
                .setTimeout(60L, SECONDS)
                .setActivity(this)
                .setCallbacks(phoneVerificationCallbacks)
                .setForceResendingToken(forceResendingToken)
                .build()

            PhoneAuthProvider.verifyPhoneNumber(options)
        }
    }

    private fun verifyPhoneNumberWithCredential(credential: PhoneAuthCredential) {
        auth.signInWithCredential(credential)
            .addOnCompleteListener(this) { task->
                if (task.isSuccessful) {
                    Log.i("phoneVerification", "Signed in with credential: $credential")
                } else {
                    Log.w("phoneVerification", "Verification failed!")
                }
            }

        launchProfileActivity()
    }

    private fun launchProfileActivity() {
        val intent = Intent(this, ProfileActivity::class.java)
        startActivity(intent)
    }
}