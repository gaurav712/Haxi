package io.github.gaurav712.haxi

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.google.firebase.auth.FirebaseAuth
import io.github.gaurav712.haxi.databinding.ActivityProfileBinding

class ProfileActivity : AppCompatActivity() {

    private lateinit var binding: ActivityProfileBinding
    private lateinit var firebaseAuth: FirebaseAuth

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityProfileBinding.inflate(layoutInflater)
        setContentView(binding.root)

        firebaseAuth = FirebaseAuth.getInstance()

        val authStateListener = FirebaseAuth.AuthStateListener {
            val user = firebaseAuth.currentUser
            if (user != null) {
                Log.i("userLogin", "User logged in")
                binding.loginInfo.text = "logged in as: ${user.phoneNumber}"
            } else {
                Log.i("userLogin", "User logged out")
            }
        }

        firebaseAuth.addAuthStateListener(authStateListener)

        binding.logoutButton.setOnClickListener {
            firebaseAuth.signOut()
        }
    }
}